// This file contains temporary utility functions.
// In future it will contain declaration of ,,string'' class.
// List of funcions:
// - bracket

#ifndef STRINGS_UTILITY_H
#define STRINGS_UTILITY_H

// Returns i-th bracket.
// Brackets are sorted in the same order as in GEN_BRACKETS.
char bracket(int i)
{
    char arr[8] = {'(', ')', '{', '}', '[', ']', '<', '>'};

    return arr[i];
}

#endif /* STRINGS_UTILITY_H */
