// This file contains functions to generate prefixes of infinite special words
// like Fibonacci or Thue Morse.
// List of funcions:
// - genFibonacciWord
// - genThueMorseWord

#ifndef STRINGS_INFINITY_H
#define STRINGS_INFINITY_H

#include <string>
#include <bits/stdc++.h>
#include "../Rnd/rnd.h"
#include "utility.h"

// Types of alphabets:
#define GEN_NUMBERS 0 // numbers
#define GEN_SMALLENG 1 // small english alphabet
#define GEN_BIGENG 2 // big english alphabet
#define GEN_BRACKETS 3 // brackets in the following order: (,),{,},[,],<,>

using std::string;

// Generates prefix of infinite Fibonacci word of fixed length.
// List of arguments:
// a) n - length of string
// b) type - type of alphabet (see defines at the beginning of the file)
string genFibonacciWord(int n, unsigned type = GEN_SMALLENG)
{
		string a, b, c, ret;

		a = "1";
		a[0] -= '0';

		b = "0";
		b[0] -= '0';

		if(n == 1)
		  ret = b;

		else
		  {
		  	  c = b + a;

		  	  while(int(c.size()) < n)
		  	    {
		  	    	c = b + a;
		  	    	a = b;
		  	    	b = c;
		  	    }

		  	  for(int i = 0; i < n; i++)
		  	    {
		  	    	ret += c[i];
		  	    }
		  }

		for(int i = 0; i < n; i++)
		  {
		  	  if(type == GEN_SMALLENG)
		  	    ret[i] += 'a';

		  	  else if(type == GEN_BIGENG)
		  	    ret[i] += 'A';

		  	  else if(type == GEN_NUMBERS)
		  	    ret[i] += '0';

					else if(type == GEN_BRACKETS)
						ret[i] = bracket(ret[i]);
		  }

		return ret;
}

// Generates prefix of infinite Thue Morse word of fixed length.
// List of arguments:
// a) n - length of string
// b) type - type of alphabet (see defines at the beginning of the file)
string genThueMorseWord(int n, unsigned type = GEN_NUMBERS)
{
		string a, ret;

		a = "0";
		a[0] -= '0';

		while(int(a.size()) < n)
		  {
		  	  int siz = a.size();

		  	  for(int i = 0; i < siz; i++)
		  	    {
		  	    	a += char(!(bool)(a[i]));
		  	    }
		  }

		for(int i = 0; i < n; i++)
		  ret += a[i];

		for(int i = 0; i < n; i++)
		  {
		  	  if(type == GEN_SMALLENG)
		  	    ret[i] += 'a';

		  	  else if(type == GEN_BIGENG)
		  	    ret[i] += 'A';

		  	  else if(type == GEN_NUMBERS)
		  	    ret[i] += '0';

					else if(type == GEN_BRACKETS)
						ret[i] = bracket(ret[i]);
		  }

		return ret;
}

#endif /* STRINGS_INFINITY_H */
