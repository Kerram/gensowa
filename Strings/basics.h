// This file contains functions to generate basic types of words.
// List of funcions:
// - genRandomString
// - genPeriodicString

#ifndef STRINGS_BASICS_H
#define STRINGS_BASICS_H

#include <string>
#include <bits/stdc++.h>

#include "../Rnd/rnd.h"
#include "utility.h"

// Types of alphabets:
#define GEN_NUMBERS 0 // numbers
#define GEN_SMALLENG 1 // small english alphabet
#define GEN_BIGENG 2 // big english alphabet
#define GEN_BRACKETS 3 // brackets in the following order: (,),{,},[,],<,>

using std::string;

// Generates random string.
// List of arguments:
// a) n - length of string
// b) type - type of alphabet (see defines at the beginning of the file)
// c) mini - minimal value of a character
// d) maxi - maximal value of a character
string genRandomString(int n, unsigned type = GEN_SMALLENG, int mini = 0, int maxi = 25)
{
		string ret;

		for(int i = 0; i < n; i++)
		  {
		  	  int a = rnd(mini, maxi);

		  	  if(type == GEN_SMALLENG)
		  	    a += 'a';

		  	  else if(type == GEN_BIGENG)
		  	    a += 'A';

		  	  else if(type == GEN_NUMBERS)
		  	    a += '0';

					if(type == GEN_BRACKETS)
						ret += bracket(a);

					else
		  	  	ret += char(a);
		  }

		return ret;
}

// Generates random periodic string.
// List of arguments:
// a) n - length of string
// b) m - length of period
// c) type - type of alphabet (see defines at the beginning of the file)
// d) mini - minimal value of a character
// e) maxi - maximal value of a character
string genPeriodicString(int n, int m, unsigned type = GEN_SMALLENG, int mini = 0, int maxi = 25)
{
		string ret;
		string period;

		for(int i = 0; i < m; i++)
		  {
		  	  int a = rnd(mini, maxi);

		  	  if(type == GEN_SMALLENG)
		  	    a += 'a';

		  	  else if(type == GEN_BIGENG)
		  	    a += 'A';

		  	  else if(type == GEN_NUMBERS)
		  	    a += '0';

					if(type == GEN_BRACKETS)
						period += bracket(a);

					else
		  	  	period += char(a);
		  }

		for(int i = 0; i < n; i++)
		  {
		  	  ret += period[i % m];
		  }

		return ret;
}

#endif /* STRINGS_BASICS_H */
