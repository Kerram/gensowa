// This file contains functions to generate polygons.
// List of functions:
// - genConvecPolygon

#ifndef GEOMETRY_POLYGON_H
#define GEOMETRY_POLYGON_H

#include <algorithm>
#include <vector>

#include "../Rnd/rnd.h"
#include "utils.h"
#include "pointset.h"

// Generates random convex polygon.
// Coordinates are in range [dx, dx + 1000] for x and [dy, dy + 10000] for y
// List of arguments:
// a) n - number of points
// b) dx - lowest possible x coordinate
// c) dy - lowest possible y coordinate
PointSet <long long> genConvexPolygon(int n, long long dx = 0, long long dy = 0)
{
    PointSet <long long> ps;

    std::vector < Point<long long> > tab, bat, fat;

    for(int i = 0; i < 1000; i++)
    {
        for(int j = 0; j < 1000; j++)
        {
            tab.push_back(Point <long long> (i, j));
        }
    }

    std::random_shuffle(tab.begin(), tab.end());

    for(int i = 0; i < n - 2; i++)
    {
        bat.push_back(tab[i]);
    }

    std::sort(bat.begin(), bat.end(), angleCmp <long long>);

    fat.push_back(Point <long long> (0, 0));
    ps.addPoint(dx, dy);

    for(int i = 1; i < n - 1; i++)
    {
        fat.push_back(fat[i - 1] + bat[i - 1]);
        ps.addPoint(fat[i] + Point<long long>(dx, dy));
    }

    ps.addPoint(Point <long long> (0, fat[n - 3].y) + Point<long long>(dx, dy));

    return ps;
}

/*PointSet genConvexHull(PointSet p)
{
    PointSet finalHull;
    stack <Point> hull;
    set <Point> S;
    Point top;
    Point base;
    int n = p.points.size();
    Point tab[n];
    Point a,b;
    for(int i=0;i<n;i++)
    {
        tab[i] = p.points[i];

    }
    sort(tab,tab+n);
    for(int i=1;i<n;i++)
    {
        tab[i].x -= tab[0].x;
        tab[i].y -= tab[0].y;
    }
    base = tab[0];
    tab[0].x = 0;
    tab[0].y = 0;
    sort(tab+1,tab+n,AngleCmp);
    hull.push(tab[0]);
    int licz=1;
    hull.push(tab[1]);
    while(Det(tab[1],tab[licz]) == 0) licz++;
    for(int i=licz;i<n-1;i++)
    {
        top = hull.top();
        hull.pop();
        a = top - hull.top();
        b = tab[i+1] - hull.top();
        hull.push(top);
        while (Det(a,b)<=0)
        {
       //     cout << 1;
            hull.pop();
            top = hull.top();
            hull.pop();
       //     cout << 2;
            a = top - hull.top();
            b = tab[i+1] - hull.top();
            hull.push(top);
         //   cout << 4;
        }
       // cout << 5;
        hull.push(tab[i+1]);
    }
    while(!hull.empty())
    {
        finalHull.AddPoint(hull.top()+base);
        hull.pop();
    }
    return finalHull;
}*/

#endif /* GEOMETRY_POLYGON_H */
