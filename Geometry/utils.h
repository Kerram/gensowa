// This file contains utility structs and functions useful in geometry.
// List of structs:
// - Point
// List of functions:
// - dist
// - sqrDist
// - countLatticePoints
// - genPoint
// - angleCmp
// - scal
// - direction
// - det

#ifndef GEOMETRY_UTILS_H
#define GEOMETRY_UTILS_H

#include <cmath>
#include <iostream>

#include "../Rnd/rnd.h"

template <typename T>
struct Point
{
    T x;
    T y;

    Point()
    {
		    x = y = 0;
	  }

    Point(T a, T b)
    {
        x = a;
        y = b;
    }

    // Comparison via the pair of coordinates.
    bool operator<(const Point &a) const
    {
        if(a.y == y)
          return x < a.x;

        return y < a.y;
    }

    // Comparison via the pair of coordinates.
    bool operator==(const Point &a) const
    {
        return (a.y == y) && (a.x == x);
    }

    // Comparison via the pair of coordinates.
    bool operator>(const Point &a) const
    {
        return a < (*this);
    }

    // Comparison via the pair of coordinates.
    bool operator<=(const Point &a) const
    {
        return (operator<(a)) || (operator==(a));
    }

    // Comparison via the pair of coordinates.
    bool operator>=(const Point &a) const
    {
        return (operator>(a)) || (operator==(a));
    }

    // Comparison via the pair of coordinates.
    bool operator!=(const Point &a) const
    {
        return !(operator==(a));
    }

    // Addition on coordinates.
    Point operator+(const Point &a) const
    {
        Point f(*this);

        f.x += a.x;
        f.y += a.y;

        return f;
    }

     // Substraction on coordinates.
    Point operator-(const Point &a) const
    {
        Point f(*this);

        f.x -= a.x;
        f.y -= a.y;

        return f;
    }

    // Addition on coordinates.
    Point operator+=(const Point &a)
    {
        (*this) = operator+(a);

        return *this;
    }

    // Substraction on coordinates.
    Point operator-=(const Point &a)
    {
        (*this) = operator-(a);

        return *this;
    }

    // Access to coordinates via numbers instead of literals.
    T& operator[](int a)
    {
		    return a == 0 ? x : y;
	  }

    // Enables writing points via ostream; format: "x y".
  	friend std::ostream& operator<<(std::ostream& out, const Point &a)
  	{
    		out<<a.x<<" "<<a.y;

    		return out;
  	}
};

// Calculates cross product of two vectors with common beginning (0, 0),
template <typename T>
T det(const Point <T> &p1, const Point <T> &p2)
{
    return (p1.x * p2.y) - (p2.x * p1.y);
}

// Calculates cross product of two vectors with common beginning p3.
template <typename T>
T direction(const Point <T> &p1, const Point <T> &p2, const Point <T> &p3)
{
	return det(p1 - p3, p2 - p3);
}

// Calculates dot product of two vectors with common beginning (0, 0).
template <typename T>
T scal(const Point <T> &a, const Point <T> &b)
{
    return (a.x * b.x) + (a.y * b.y);
}

// Comparator for 'angle sort'.
template <typename T>
bool angleCmp(const Point <T> &a, const Point <T> &b)
{
    if(det(a, b) == 0)
      return scal(a, a) < scal(b, b);

    return det(a,b) > 0;
}

// Generates random point.
template <typename T>
Point <T> genPoint(T minX, T maxX, T minY, T maxY)
{
    return Point <T> (rnd(minX, maxX), rnd(minY, maxY));
}

// Counts lattice points on segment (p1 - p2) with integer coordinates.
template <typename T>
long long countLatticePoints(const Point <T> &p1, const Point <T> &p2)
{
    return __gcd(std::abs(p2.x - p1.x), std::abs(p2.y - p1.y));
}

// Returns square of distance between two points.
template <typename T>
long double sqrDist(const Point <T> &p1,const Point <T> &p2)
{
    return sqr(p1.x - p2.x) + sqr(p1.y - p2.y);
}

// Returns square of distance betweem two points.
template <typename T>
long double dist(const Point <T> &p1, const Point <T> &p2)
{
    return sqrt(SqrDist(p1, p2));
}

#endif /* GEOMETRY_UTILS_H */
