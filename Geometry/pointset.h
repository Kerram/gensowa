// Class containing data about set of points.
// It stores vector of points and can apply several operations on it.
// For further details check implementation.

#ifndef GEOMETRY_POINT_SET_H
#define GEOMETRY_POINT_SET_H

#include <vector>
#include <iostream>

#include "utils.h"

template <typename T>
class PointSet
{
		private:
    std::vector < Point<T> > points;

    public:
		 // Returns number of points.
    int size() const
    {
        return points.size();
    }

		// Adds new point.
    void addPoint(T x, T y)
    {
        addPoint(Point <T> (x, y));
    }

		// Adds new point.
    void addPoint(const Point <T> &p)
    {
        points.push_back(p);
    }

		// Writes points (from number a to b) on the standard output in format:
		// "x y\n" - for each point.
    void write(int a = 0, int b = size())
    {
        for(int i = a; i < b; i++)
          {
             std::cout<<points[i].x<<" "<<points[i].y<<std::endl;
          }
    }

		// Returns vector of points (from number a to b).
    std::vector < Point<T> > getPoints(int a = 0, int b = size())
    {
				std::vector < Point<T> > temp;

				for(int i = a; i < b; i++)
				  temp.push_back(points[i]);

				return temp;
		}
};

#endif /* GEOMETRY_POINT_SET_H */
