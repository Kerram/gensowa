// This file contains function to generate sets of points.
// List of functions:
// - genPoints

#ifndef GEOMETRY_POINTS_H
#define GEOMETRY_POINTS_H

#include <set>

#include "utils.h"
#include "pointset.h"

// Generate random set of points.
// List of arguments:
// a) n - number of points
// b) minX - minimal x coordinate
// c) maxX - maximal x coordinate
// d) minY - minimal y coordinate
// e) maxY - maximal y coordinate
// f) unique - flag whether set of points is set(unique == true) or
// multiset(unique == false)
PointSet <int> genPoints(int n, int minX, int maxX, int minY, int maxY, bool unique = true)
{
		std::set < Point<int> > seti;
		PointSet <int> ans;

		for(int i = 0; i < n; i++)
		  {
				  Point <int> tmp = genPoint(minX, maxX, minY, maxY);

				  if(!unique)
				    {
								ans.addPoint(tmp);
								continue;
						}

				  while(seti.count(tmp) == 1)
				    {
								tmp = genPoint(minX, maxX, minY, maxY);
						}

				  ans.addPoint(tmp);
				  seti.insert(tmp);
		  }

		return ans;
}

#endif /* GEOMETRY_POINTS_H */
