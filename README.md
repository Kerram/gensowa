# README #

### How does it work? ###

* All files of the library are stored in "GenSowa" directory. The library is divided into sections, for example: graphs, geometry, sequences.
* Each section has its own summary file, that includes everything and provides short table of contents and directory called as its section (started from the capital letter). Generators are divided into several files, like: tree, DAG, planar. Each of them is stored in the inner directory and provides some useful(I hope so) functions. Each function must be documented in its header file. In main directory there is a file called test.cpp, which is useful to test our code.
* The library is 'header-only', because of the requirements of SOWA. If you have any ideas about the concept, feel free to share it in this file.

### TO DO ###

* New sections
* Documentation in headers
* Real documentation (maybe doxygen)
* Any new ideas
* Think whether invers should be stored in their own section or in classes declarations (as it is now)
* Think on putting everything in the appropriate namespace.
* Create a new directory for examples (or put them in the corresponding sections)
* Unify and improve style of the code
* In Graphs:
  * New functions in tree.h: genNadol, genChristmas(Tree), genKnary, genRandom(add maximal degree)
  * New functions in dag.h: genConnectedDAG
  * Check if deterministic approach is faster in dealing with multiple edges in genDAG in dag.h
  * New functions in directed.h: genDEuler, genDBipartite
  * New functions in undirected.h: genUDEuler, genUDBipartite, genUDTreeOfBiconnected
  * New functions in planar.h: genPlanar
  * In graph.h: Make it dynamical in terms of vertices, Get rid of private Find & Union in isTree (maybe lambda?)
* In Rnd:
  * New functions in rnd.h: rndl, rndf, rndlf
* In Geometry:
  * New functions in polygon.h: makeConvexHull
  * Test Everything
*  In Strings:
  * Change the idea of alphabets (maybe generate your own from the existing ones)
  * Test Everything
  * Write own class instead of using std::string (maybe class for alphabets)
  * Maybe change the name of this section
  * Get rid of utility header.
When you do sth from 'TO DO' list, erase it and write below.

### DONE ###

* Sections:
  * Graphs:
    * graph: declaration of class Graph
    * tree: genTree, genPath, genSun, genBroom, genDoubleSun, genBinary, genComb, genForest
    * dag: genDAG
    * directed: genDRandom (requires more tests)
    * undirected: genUDJellyfish, genUDRandom, genUDBiconnected
    * planar: (empty)
  * Rnd:
    * rnd: rnd, rndll, sqr
  * Geometry (requires more tests):
    * utils: declaration of struct Point, det, direction, scal, angleCmp, genPoint, countPoints, sqrDist, dist
    * pointset: declaration of class PointSet
    * polygon: genConvexPolygon
    * points: genPoints
  * Strings (requires more tests, but less than Geometry):
    * basics: genRandomString, genPeriodicString
    * infinity: genFibonacciWord, genThueMorseWord
    * utility: bracket
  * Sequences:
    * sequence: declaration of class Sequence
    * basics: genRandomSeq

### What is this repository for? ###

* The goal is to make easy to use library, which will improve writing generators in SOWA.
* Version: Pre-Alpha
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
