// Summary file for section Graphs.

#ifndef GRAPHS_H
#define GRAPHS_H

#include "Graphs/tree.h"
#include "Graphs/dag.h"
#include "Graphs/undirected.h"
#include "Graphs/directed.h"
#include "Graphs/planar.h"

#endif /* GRAPHS_H */
