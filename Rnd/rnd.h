// This file contains several utility functions.
// List of functions:
// - rnd
// - sqr
// - rndll

#ifndef RND_RND_H
#define RND_RND_H

#include <cstdlib>

// Returns random int from range [a, b].
int rnd(int a, int b)
{
	return (rand() % (b - a + 1)) + a;
}

// Returns square of the argument if its type has operator*.
template <typename T>
T sqr(const T &a)
{
	return a * a;
}

// Returns random long long from range [a, b].
long long rndll(long long a, long long b)
{
	long long x = rand() % 1000000000;
	long long y = rand() % 1000000000;

	x *= 1000000000;
	x += y;

	return (x % (b - a + 1)) + a;
}

#endif /* RND_RND_H */
