// This file contains functions to generate DAGs.
// List of funcions:
// - genDAG

#ifndef GRAPHS_DAG_H
#define GRAPHS_DAG_H

#include <map>

#include "graph.h"

// Generates random DAG.
// List of arguments:
// a) n - number of vertices
// b) m - number of edges
// c) weighted_edges - flag whether graph has weighted edges
// d) lo_e - minimal weight of an edge
// e) up_e - maximal weight of an edge
// f) weighted_vertices - flag whether graph has weighted vertices
// g) lo_v - minimal weight of a vertex
// h) up_v - maximal weight of a vertex
Graph genDAG(long long n, int m, bool weighted_edges = false, int lo_e = 0, int up_e = 0, bool weighted_vertices = false, int lo_v = 0, int up_v = 0)
{
    m = min((long long) m, (n * (n - 1)) / 2);

    int a, b;
    map <pair<int, int>, bool> edges;

    Graph DAG(n, true);

    for(int i = 0; i < m; i++)
      {
          do
            {
                a = (rand() % n) + 1;
                b = (rand() % (n - 1)) + 1;

                if(a == b)
                  b = n;

                if(a > b)
                  swap(a, b);

            }while(edges[make_pair(a, b)]);

          edges[make_pair(a, b)] = true;

          DAG.addEdge(a, b);
      }

    DAG.shuffle();

    if(weighted_edges)
      DAG.weightedEdges(lo_e, up_e);

    if(weighted_vertices)
      DAG.weightedVertices(lo_v, up_v);

    return DAG;
}

#endif /* GRAPHS_DAG_H */
