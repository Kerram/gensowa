// This file contains functions to generate directed graphs.
// List of funcions:
// - genDRandom

#ifndef GRAPHS_DIRECTED_H
#define GRAPHS_DIRECTED_H

#include <map>

#include "graph.h"

// Generates random directed graph.
// List of arguments:
// a) n - number of vertices
// b) m - number of edges
// c) weighted_edges - flag whether graph has weighted edges
// d) lo_e - minimal weight of an edge
// e) up_e - maximal weight of an edge
// f) weighted_vertices - flag whether graph has weighted vertices
// g) lo_v - minimal weight of a vertex
// h) up_v - maximal weight of a vertex
Graph genDRandom(long long n, int m, bool weighted_edges = false, int lo_e = 0, int up_e = 0, bool weighted_vertices = false, int lo_v = 0, int up_v = 0)
{
    int a, b;
    map <pair<int, int>, bool> edges;

    Graph G(n, true);

    for(int i = 0; i < m; i++)
      {
          do
            {
                a = (rand() % n) + 1;
                b = (rand() % (n - 1)) + 1;

                if(a == b)
                  b = n;

            }while(edges[make_pair(a, b)]);

          edges[make_pair(a, b)] = true;

          G.addEdge(a, b);
      }

    G.shuffle();

    if(weighted_edges)
      G.weightedEdges(lo_e, up_e);

    if(weighted_vertices)
      G.weightedVertices(lo_v, up_v);

    return G;
}

#endif /* GRAPHS_DIRECTED_H */
