// Main class containing all data about the graph.
// It stores edges as vector of (beginning, end, weight).
// It supports weighted vertices and directed arcs.
// For further details check implementation.

#ifndef GRAPHS_GRAPH_H
#define GRAPHS_GRAPH_H

#include <algorithm>
#include <vector>
#include <cstring>
#include <cstdio>

#include "../Rnd/rnd.h"

using namespace std;

class Graph
{
    private:
    typedef vector< pair< pair<int, int>, int> > v2;

    int n; // number of vertices
    bool directed; // Flag whether graph is directed.
    int* vertices; // array of weights of vertices (only integers)
    v2 edges; // edges of the graph

    // Temporary functions to Find & Union (to be removed).
    int find(int, int*) const;
    void unionn(int, int, int*,int*) const;
    void makeSet(int, int*, int*) const;

    public:

    // CONSTRUCTORS & DESTRUCTORS
    // Default constructor of undirected graph.
    Graph();
    // Constructor with fixed number of vertices.
    Graph(int, bool is_directed = false);
    // Copy constructor.
    Graph(const Graph&);
    //Default destructor.
    ~Graph();

    // SELECTORS & IO FUNCTIONS
    // Returns number of vertices.
    int getN() const;
    // Returns number of edges.
    int getM() const;
    // Returns list of edges.
    v2 getEdges() const;
    // Returns whether graph is directed.
    bool isDirected() const;
    // Returns whether graph is a tree.
    bool isTree() const;
    // Writes graph on standart output; format description in definition.
    void write(bool only_n = false, bool weighted_edges = false, bool weighted_vertices = false) const;

    // MODIFIERS
    // Assignment operator.
    Graph& operator=(const Graph&);
    // Adds a new edge.
    void addEdge(int, int, int weight = 0);
    // Random shuffles list of edges.
    void shuffle();
    // Randomly assigns weights from range [a,b] to the edges.
    void weightedEdges(int, int);
    // Randomly assigns weights from range [a,b] to the vertices.
    void weightedVertices(int, int);
    // Merges two graphs; vertices have old numbers if they were in
    // original graph and shifted by n otherwise.
    void merge(const Graph&);
};

Graph::Graph()
{
    n = 0;
    directed = false;
}

Graph::Graph(int m_n, bool is_directed)
{
    n = m_n;
    directed = is_directed;

    if(n > 0)
      vertices = new int[n];
}

Graph::Graph(const Graph &g)
{
    n = g.n;
    directed = g.directed;
    edges = g.edges;

    if(n > 0)
      vertices = new int[n];

    for(int i = 0; i < n; i++)
      vertices[i] = g.vertices[i];
}

Graph& Graph::operator=(const Graph &g)
{
    if(this == &g)
      return *this;

    if(n > 0)
      delete[] vertices;

    n = g.n;
    directed = g.directed;
    edges = g.edges;

    if(n > 0)
      vertices = new int[n];

    for(int i = 0; i < n; i++)
      vertices[i] = g.vertices[i];

    return *this;
}

Graph::~Graph()
{
    if(n > 0)
      delete[] vertices;
}

int Graph::getN() const
{
    return n;
}

int Graph::getM() const
{
    return edges.size();
}

vector< pair< pair<int, int>, int> > Graph::getEdges() const
{
    return edges;
}

// Format description:
// First line: number_of_vertices number_of_edges (if only_n == false)
// Second line (optional): list of weights of vertices
// (all separated by whitespace characters)
// Next lines: list of edges; each one in separate lines
// Edge contains beginning, end and optionally weight
// (all separated by whitespace characters)
void Graph::write(bool only_n, bool weighted_edges, bool weighted_vertices) const
{
    int m = edges.size();

    if(only_n)
      printf("%d\n", n);

    else
      printf("%d %d\n", n, m);

    if(weighted_vertices)
      {
          for(int i = 0; i < n - 1; i++)
            {
                printf("%d ", vertices[i]);
            }

          if(n > 0)
            printf("%d\n", vertices[n - 1]);
      }

    for(int i = 0; i < m; i++)
      {
          if(weighted_edges)
            printf("%d %d %d\n", edges[i].first.first, edges[i].first.second, edges[i].second);

          else
            printf("%d %d\n", edges[i].first.first, edges[i].first.second);
      }
}

void Graph::addEdge(int a, int b, int c)
{
    edges.push_back(make_pair(make_pair(a, b), c));
}

void Graph::shuffle()
{
    if(n == 0)
      return;

    int* per;
    per = new int[n];

    for(int i = 0; i < n; i++)
      per[i] = i + 1;

    random_shuffle(per, per + n);

    for(int i = 0; i < int(edges.size()); i++)
      {
          edges[i].first.first = per[edges[i].first.first - 1];
          edges[i].first.second = per[edges[i].first.second - 1];

          if((!directed) && (rand() % 2 == 0))
            swap(edges[i].first.first, edges[i].first.second);
      }

    random_shuffle(edges.begin(), edges.end());

    delete[] per;
}

bool Graph::isDirected() const
{
    return directed;
}

void Graph::weightedEdges(int a, int b)
{
    for(int i = 0; i < int(edges.size()); i++)
      {
          edges[i].second = rnd(a, b);
      }
}

void Graph::weightedVertices(int a, int b)
{
    for(int i = 0; i < n; i++)
      {
          vertices[i] = rnd(a, b);
      }
}

void Graph::merge(const Graph &g)
{
    if(g.n == 0)
      return;

    int* tmp = new int[max(n, 1)];

    if(n > 0)
      {
          memcpy(tmp, vertices, sizeof(int[n]));
          delete[] vertices;
      }

    vertices = new int[max(n + g.n, 1)];

    if(n > 0)
      memcpy(vertices, tmp, sizeof(int[n]));

    memcpy(vertices + n, g.vertices, sizeof(int[g.n]));

    delete[] tmp;

    pair< pair<int, int>, int> tmp2;

    for(int i = 0; i < int(g.edges.size()); i++)
      {
          tmp2 = g.edges[i];

          tmp2.first.first += n;
          tmp2.first.second += n;

          edges.push_back(tmp2);
      }

    n += g.n;
}

bool Graph::isTree() const
{
    if(n <= 1)
      return true;

    if(isDirected())
      return false;

    if(int(edges.size()) + 1 != n)
      return false;

    int* rep = new int[n + 1];
    int* siz = new int[n + 1];
    int count = n;

    for(int i = 0; i <= n; i++)
      makeSet(i, rep, siz);

    for(int i = 0; i < int(edges.size()); i++)
      {
          if(find(edges[i].first.first, rep) != find(edges[i].first.second, rep))
            {
                count--;
                unionn(edges[i].first.first, edges[i].first.second, rep, siz);
            }
      }

    delete[] rep;
    delete[] siz;

    return count == 1;
}

int Graph::find(int a, int* rep) const
{
    if(rep[a] != a)
      rep[a] = find(rep[a], rep);

    return rep[a];
}

void Graph::unionn(int a, int b, int* rep, int* siz) const
{
    int ra = find(a, rep), rb = find(b, rep);

    if(siz[ra] < siz[rb])
      rep[ra] = rb;

    else
      {
          rep[rb] = ra;

          if(siz[ra] == siz[rb])
            siz[ra]++;
      }
}

void Graph::makeSet(int a, int* rep, int* siz) const
{
    rep[a] = a;
    siz[a] = 1;
}

#endif /* GRAPHS_GRAPH_H */
