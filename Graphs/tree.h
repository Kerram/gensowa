// This file contains function to generate trees.
// List of funcions:
// - genTree
// - genSun
// - genBroom
// - genPath
// - genDoubleSun
// - genBinary
// - genForest
// - genComb

#ifndef GRAPHS_TREE_H
#define GRAPHS_TREE_H

#include "graph.h"
#include "../Rnd/rnd.h"

#define GEN_ALL 127
#define GEN_TREE 1
#define GEN_SUN 2
#define GEN_DOUBLESUN 4
#define GEN_BROOM 8
#define GEN_BINARY 16
#define GEN_PATH 32
#define GEN_COMB 64

// Generates random tree.
// List of arguments:
// a) n - number of vertices
// b) weighted_edges - flag whether graph has weighted edges
// c) lo_e - minimal weight of an edge
// d) up_e - maximal weight of an edge
// e) weighted_vertices - flag whether graph has weighted vertices
// f) lo_v - minimal weight of a vertex
// g) up_v - maximal weight of a vertex
Graph genTree(int n, bool weighted_edges = false, int lo_e = 0, int up_e = 0, bool weighted_vertices = false, int lo_v = 0, int up_v = 0)
{
    Graph tree(n);

    for(int i = 2; i <= n; i++)
      {
          tree.addEdge(i, (rand() % (i - 1)) + 1);
      }

    tree.shuffle();

    if(weighted_edges)
      tree.weightedEdges(lo_e, up_e);

    if(weighted_vertices)
      tree.weightedVertices(lo_v, up_v);

    return tree;
}

// Generates random sun.
// List of arguments:
// a) n - number of vertices
// b) weighted_edges - flag whether graph has weighted edges
// c) lo_e - minimal weight of an edge
// d) up_e - maximal weight of an edge
// e) weighted_vertices - flag whether graph has weighted vertices
// f) lo_v - minimal weight of a vertex
// g) up_v - maximal weight of a vertex
Graph genSun(int n, bool weighted_edges = false, int lo_e = 0, int up_e = 0, bool weighted_vertices = false, int lo_v = 0, int up_v = 0)
{
    Graph tree(n);

    for(int i = 2; i <= n; i++)
      {
          tree.addEdge(i, 1);
      }

    tree.shuffle();

    if(weighted_edges)
      tree.weightedEdges(lo_e, up_e);

    if(weighted_vertices)
      tree.weightedVertices(lo_v, up_v);

    return tree;
}

// Generates random path.
// List of arguments:
// a) n - number of vertices
// b) weighted_edges - flag whether graph has weighted edges
// c) lo_e - minimal weight of an edge
// d) up_e - maximal weight of an edge
// e) weighted_vertices - flag whether graph has weighted vertices
// f) lo_v - minimal weight of a vertex
// g) up_v - maximal weight of a vertex
Graph genPath(int n, bool weighted_edges = false, int lo_e = 0, int up_e = 0, bool weighted_vertices = false, int lo_v = 0, int up_v = 0)
{
    Graph tree(n);

    for(int i = 2; i <= n; i++)
      {
          tree.addEdge(i, i - 1);
      }

    tree.shuffle();

    if(weighted_edges)
      tree.weightedEdges(lo_e, up_e);

    if(weighted_vertices)
      tree.weightedVertices(lo_v, up_v);

    return tree;
}

// Generates random broom.
// List of arguments:
// a) n - number of vertices
// b) part - number of vertices on a path
// c) weighted_edges - flag whether graph has weighted edges
// d) lo_e - minimal weight of an edge
// e) up_e - maximal weight of an edge
// f) weighted_vertices - flag whether graph has weighted vertices
// g) lo_v - minimal weight of a vertex
// h) up_v - maximal weight of a vertex
Graph genBroom(int n, int part, bool weighted_edges = false, int lo_e = 0, int up_e = 0, bool weighted_vertices = false, int lo_v = 0, int up_v = 0)
{
    part = max(part, 1);
    part = min(part, n);

    Graph tree(n);

    for(int i = 2; i <= part; i++)
      {
          tree.addEdge(i, 1);
      }

    for(int i = part + 1; i <= n; i++)
      {
          if(i == part + 1)
            {
                tree.addEdge(i, 1);
                continue;
            }

          tree.addEdge(i, i - 1);
      }

    tree.shuffle();

    if(weighted_edges)
      tree.weightedEdges(lo_e, up_e);

    if(weighted_vertices)
      tree.weightedVertices(lo_v, up_v);

    return tree;
}

// Generetes two random suns connected by an edge from center to center.
// List of arguments:
// a) n - number of vertices of first sun
// b) m - number of vertices of second sun
// c) weighted_edges - flag whether graph has weighted edges
// d) lo_e - minimal weight of an edge
// e) up_e - maximal weight of an edge
// f) weighted_vertices - flag whether graph has weighted vertices
// g) lo_v - minimal weight of a vertex
// h) up_v - maximal weight of a vertex
Graph genDoubleSun(int n, int m, bool weighted_edges = false, int lo_e = 0, int up_e = 0, bool weighted_vertices = false, int lo_v = 0, int up_v = 0)
{
    Graph tree(n + m);

    if(n < m)
      swap(n, m);

    if(m > 0)
      tree.addEdge(1, n + 1);

    for(int i = 2; i <= n; i++)
      {
          tree.addEdge(i, 1);
      }

    for(int i = n + 2; i <= n + m; i++)
      {
          tree.addEdge(i, n + 1);
      }

    tree.shuffle();

    if(weighted_edges)
      tree.weightedEdges(lo_e, up_e);

    if(weighted_vertices)
      tree.weightedVertices(lo_v, up_v);

    return tree;
}

// Generates perfect (or almost depending on number of vertices) binary tree
// from up to down and from left to right.
// List of arguments:
// a) n - number of vertices
// b) weighted_edges - flag whether graph has weighted edges
// c) lo_e - minimal weight of an edge
// d) up_e - maximal weight of an edge
// e) weighted_vertices - flag whether graph has weighted vertices
// f) lo_v - minimal weight of a vertex
// g) up_v - maximal weight of a vertex
Graph genBinary(int n, bool weighted_edges = false, int lo_e = 0, int up_e = 0, bool weighted_vertices = false, int lo_v = 0, int up_v = 0)
{
    Graph tree(n);

    for(int i = 2; i <= n; i++)
      {
          tree.addEdge(i, i / 2);
      }

    tree.shuffle();

    if(weighted_edges)
      tree.weightedEdges(lo_e, up_e);

    if(weighted_vertices)
      tree.weightedVertices(lo_v, up_v);

    return tree;
}

// Generates random comb.
// List of arguments:
// a) n - number of vertices
// b) length - the length of the part (number of nodes per tooth is the same,
// save for the last)
// c) type - set of trees' types, which can be used in teeth;
// if you want to add type than 'or' your flag with constants
// listed at the beginning of this file
// d) weighted_edges - flag whether graph has weighted edges
// e) lo_e - minimal weight of an edge
// f) up_e - maximal weight of an edge
// g) weighted_vertices - flag whether graph has weighted vertices
// h) lo_v - minimal weight of a vertex
// i) up_v - maximal weight of a vertex
Graph genComb(int n, int length, unsigned int type = GEN_ALL, bool weighted_edges = false, int lo_e = 0, int up_e = 0, bool weighted_vertices = false, int lo_v = 0, int up_v = 0)
{
    length = max(1, length);

    Graph tree(length);

    for(int i = 1; i < length; i++)
      tree.addEdge(i, i + 1);

    int tooth = n - length;
    tooth /= length;

    int* types = new int[10];
    int count = 0;

    for(int i = 0; i < 10; i++)
      {
          if((type & (1 << i)) != 0)
            types[count] = (1 << i), count++;
      }

    Graph tmp;

    for(int i = 1; i <= length; i++)
      {
          int nodes = tooth;

          if(i == length)
            nodes += (n - length) % length;

          if(nodes == 0)
            break;

          int a = rnd(0, count - 1);

          if(types[a] == GEN_TREE)
            tmp = genTree(nodes, weighted_edges, lo_e, up_e, weighted_vertices, lo_e, up_e);

          else if(types[a] == GEN_SUN)
            tmp = genSun(nodes, weighted_edges, lo_e, up_e, weighted_vertices, lo_v, up_v);

          else if(types[a] == GEN_PATH)
            tmp = genPath(nodes, weighted_edges, lo_e, up_e, weighted_vertices, lo_v, up_v);

          else if(types[a] == GEN_BINARY)
            tmp = genBinary(nodes, weighted_edges, lo_e, up_e, weighted_vertices, lo_v, up_v);

          else if(types[a] == GEN_DOUBLESUN)
            {
                a = rnd(0, nodes);
                tmp = genDoubleSun(nodes - a, a, weighted_edges, lo_e, up_e, weighted_vertices, lo_v, up_v);
            }

          else if(types[a] == GEN_BROOM)
            {
                a = rnd(1, nodes);
                tmp = genBroom(nodes, a, weighted_edges, lo_e, up_e, weighted_vertices, lo_v, up_v);
            }

          else if(types[a] == GEN_COMB)
            {
                a = rnd(1, nodes);
                tmp = genComb(nodes, a, GEN_ALL, weighted_edges, lo_e, up_e, weighted_vertices, lo_v, up_v);
            }

          int old = tree.getN();

          tree.merge(tmp);
          tree.addEdge(i, old + 1);
      }

    tree.shuffle();

    if(weighted_edges)
      tree.weightedEdges(lo_e, up_e);

    if(weighted_vertices)
      tree.weightedVertices(lo_v, up_v);

    delete types;

    return tree;
}

// Generates forest.
// List of arguments:
// a) n - number of vertices
// b) trees - number of trees (if -1 then random)
// c) type - set of trees' types, which can be used in the forest;
// if you want to add type than 'or' your flag with constants
// listed at the beginning of this file
// d) weighted_edges - flag whether graph has weighted edges
// e) lo_e - minimal weight of an edge
// f) up_e - maximal weight of an edge
// g) weighted_vertices - flag whether graph has weighted vertices
// h) lo_v - minimal weight of a vertex
// i) up_v - maximal weight of a vertex
Graph genForest(int n, int trees = -1, unsigned int type = GEN_ALL, bool weighted_edges = false, int lo_e = 0, int up_e = 0, bool weighted_vertices = false, int lo_v = 0, int up_v = 0)
{
    Graph forest(0);

    if(trees == -1)
      trees = rnd(1, n);

    trees = min(trees, n);
    type = max((unsigned int)GEN_TREE, type);

    int* sizes = new int[trees];
    int tmp = n - trees, a;

    for(int i = 0; i < trees - 1; i++)
      {
          a = rnd(0, tmp);

          sizes[i] = 1 + a;

          tmp -= a;
      }

    sizes[trees - 1] = tmp + 1;
    int bits = __builtin_popcount(type);

    int per[10];
    a = 0;

    for(int i = 0; i < 10; i++)
      {
          if((type & (1u << i)) == (1u << i))
            {
                per[a] = (1 << i);
                a++;
            }
      }

    for(int i = 0; i < trees; i++)
      {
          a = rnd(0, bits - 1);

          if(per[a] == GEN_TREE)
            {
                forest.merge(genTree(sizes[i], weighted_edges, lo_e, up_e, weighted_vertices, lo_v, up_v));
            }

          else if(per[a] == GEN_SUN)
            {
                forest.merge(genSun(sizes[i], weighted_edges, lo_e, up_e, weighted_vertices, lo_v, up_v));
            }

          else if(per[a] == GEN_PATH)
            {
                forest.merge(genPath(sizes[i], weighted_edges, lo_e, up_e, weighted_vertices, lo_v, up_v));
            }

          else if(per[a] == GEN_BINARY)
            {
                forest.merge(genBinary(sizes[i], weighted_edges, lo_e, up_e, weighted_vertices, lo_v, up_v));
            }

          else if(per[a] == GEN_DOUBLESUN)
            {
                a = rnd(0, sizes[i] - 1);

                forest.merge(genDoubleSun(sizes[i] - a, a, weighted_edges, lo_e, up_e, weighted_vertices, lo_v, up_v));
            }

          else if(per[a] == GEN_BROOM)
            {
                a = rnd(1, sizes[i]);

                forest.merge(genBroom(sizes[i], a, weighted_edges, lo_e, up_e, weighted_vertices, lo_v, up_v));
            }

          else if(per[a] == GEN_COMB)
            {
                a = rnd(1, sizes[i]);

                forest.merge(genComb(sizes[i], a, GEN_ALL, weighted_edges, lo_e, up_e, weighted_vertices, lo_v, up_v));
            }
      }

    forest.shuffle();

    delete[] sizes;

    return forest;
}

#endif /* GRAPHS_TREE_H */
