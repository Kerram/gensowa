// This file contains functions to generate undirected graphs.
// List of funcions:
// - genUDJellyFish
// - genUDRandom
// - genBiconnected

#ifndef GRAPHS_UNDIRECTED_H
#define GRAPHS_UNDIRECTED_H

#include <vector>
#include <cstring>
#include <map>

#include "graph.h"

// Generates random jellyfish.
// List of arguments:
// a) n - number of vertices
// b) size - number of vertices on cycle
// (if -1 then size if random, but greater than two)
// c) weighted_edges - flag whether graph has weighted edges
// d) lo_e - minimal weight of an edge
// e) up_e - maximal weight of an edge
// f) weighted_vertices - flag whether graph has weighted vertices
// g) lo_v - minimal weight of a vertex
// h) up_v - maximal weight of a vertex
Graph genUDJellyfish(int n, int size = -1, bool weighted_edges = false, int lo_e = 0, int up_e = 0, bool weighted_vertices = false, int lo_v = 0, int up_v = 0)
{
    n = max(n, 3);

    Graph graph(n);

    if(size == -1)
      {
          int a = (rand() % n) + 1, b;
          int count = 1;

          bool is[n + 1];
          memset(&is, true, sizeof(is));

          is[a] = false;

          for(int i = 2; i <= n; i++)
            {
                b = (rand() % (i - 1)) + 1;

                if(i == a)
                  is[b] = false, count++;

                if(a == b)
                  is[i] = false, count++;

                graph.addEdge(i, b);
            }

          if(count == n)
            graph.addEdge((a % n) + 1, ((a + 1) % n) + 1);

          else
            {
                for(int i = 1; i <= n; i++)
                  {
                      if(is[i])
                        {
                            graph.addEdge(a, i);
                            break;
                        }
                  }
            }
      }

    else
      {
          size = min(size, n);

          for(int i = 2; i <= size; i++)
            {
                graph.addEdge(i, i - 1);
            }

          graph.addEdge(1, size);

          for(int i = size + 1; i <= n; i++)
            {
                graph.addEdge(i, (rand() % (i - 1)) + 1);
            }
      }

    graph.shuffle();

    if(weighted_edges)
      graph.weightedEdges(lo_e, up_e);

    if(weighted_vertices)
      graph.weightedVertices(lo_v, up_v);

    return graph;
}

// Generates random undirected graph.
// List of arguments:
// a) n - number of vertices
// b) m - number of edges
// c) weighted_edges - flag whether graph has weighted edges
// d) lo_e - minimal weight of an edge
// e) up_e - maximal weight of an edge
// f) weighted_vertices - flag whether graph has weighted vertices
// g) lo_v - minimal weight of a vertex
// h) up_v - maximal weight of a vertex
Graph genUDRandom(long long n, int m, bool weighted_edges = false, int lo_e = 0, int up_e = 0, bool weighted_vertices = false, int lo_v = 0, int up_v = 0)
{
    int a, b;
    map <pair<int, int>, bool> edges;

    Graph G(n);

    for(int i = 0; i < m; i++)
      {
          do
            {
                a = (rand() % n) + 1;
                b = (rand() % (n - 1)) + 1;

                if(a == b)
                  b = n;

            }while(edges[make_pair(max(a, b), min(a, b))]);

          edges[make_pair(max(a, b), min(a, b))] = true;

          G.addEdge(a, b);
      }

    G.shuffle();

    if(weighted_edges)
      G.weightedEdges(lo_e, up_e);

    if(weighted_vertices)
      G.weightedVertices(lo_v, up_v);

    return G;
}

// Generates random Biconnected graph.
// List of arguments:
// a) n - number of vertices
// b) m - number of edges
// c) weighted_edges - flag whether graph has weighted edges
// d) lo_e - minimal weight of an edge
// e) up_e - maximal weight of an edge
// f) weighted_vertices - flag whether graph has weighted vertices
// g) lo_v - minimal weight of a vertex
// h) up_v - maximal weight of a vertex
Graph genUDBiconnected(long long n, int m, bool weighted_edges = false, int lo_e = 0, int up_e = 0, bool weighted_vertices = false, int lo_v = 0, int up_v = 0)
{
    int a, b;
    map <pair<int, int>, bool> edges;

    Graph G(n);

    if(n == 1)
      {
  			  G.shuffle();

  			  if(weighted_edges)
  				G.weightedEdges(lo_e, up_e);

  			  if(weighted_vertices)
  				G.weightedVertices(lo_v, up_v);

  			  return G;
      }

    if(n == 2)
      {
  			  G.addEdge(1, 2);
  			  G.shuffle();

  			  if(weighted_edges)
  				G.weightedEdges(lo_e, up_e);

  			  if(weighted_vertices)
  				G.weightedVertices(lo_v, up_v);

  			  return G;
      }

    int cycle = rnd(3, n);

    if(n == m)
      cycle = n;

    int wsk = cycle + 1;

    for(int i = 0; i < cycle; i++)
      {
  			  G.addEdge(i + 1, ((i + 1) % cycle) + 1);
  			  edges[make_pair(max(i + 1, ((i + 1) % cycle) + 1), min(i + 1, ((i + 1) % cycle) + 1))] = true;
      }

		int ears;

		if(n == m)
		  ears = 0;

		else
		  ears = rnd(1, m - n);

		m -= n;

		for(int i = 0; i < ears; i++)
		  {
  			  if(wsk > n)
  			    break;

  			  int leng = rnd(wsk, n);

  			  if(i == ears - 1)
  			    leng = n;

  			  a = rnd(1, cycle);
  			  b = rnd(1, cycle - 1);
  			  m--;

  			  if(b == a)
  			    b = cycle;

  			  G.addEdge(a, wsk);
  			  edges[make_pair(max(a, wsk), min(a, wsk))] = true;

  			  for(; wsk < leng; wsk++)
  			    {
      					G.addEdge(wsk, wsk + 1);
      					edges[make_pair(max(wsk, wsk + 1), min(wsk, wsk + 1))] = true;
  				  }

  			  wsk++;
  			  G.addEdge(leng, b);
  			  edges[make_pair(max(leng, b), min(leng, b))] = true;
		  }

    for(int i = 0; i < m; i++)
      {
          do
            {
                a = (rand() % n) + 1;
                b = (rand() % (n - 1)) + 1;

                if(a == b)
                  b = n;

            }while(edges[make_pair(max(a, b), min(a, b))]);

          edges[make_pair(max(a, b), min(a, b))] = true;

          G.addEdge(a, b);
      }

    G.shuffle();

    if(weighted_edges)
      G.weightedEdges(lo_e, up_e);

    if(weighted_vertices)
      G.weightedVertices(lo_v, up_v);

    return G;
}

#endif /* GRAPHS_UNDIRECTED_H */
