// This file contains functions to generate basic types of sequences.
// List of funcions:
// - genRandomSeq

#ifndef SEQUENCES_BASICS_H
#define SEQUENCES_BASICS_H

#include "sequence.h"
#include "../Rnd/rnd.h"

// Generates random sequence of ints.
// List of arguments:
// a) n - size of sequence
// b) mini - minimal element
// c) maxi - maximal element
Sequence<int> genRandomSeq(int n, int mini, int maxi)
{
    Sequence <int> seq;

    for(int i = 0; i < n; i++)
      seq.push_back(rnd(mini, maxi));

    return seq;
}

#endif /* SEQUENCES_BASICS_H */
