// Class containing all data about the sequence.
// It stores double linked list containing all elements of the sequence.
// For further details check implementation.

#ifndef SEQUENCES_SEQUENCE_H
#define SEQUENCES_SEQUENCE_H

#include <iostream>
#include <cstdio>
#include <list>

template <class T>
class Sequence
{
    private:
    std::list <T> seq; //sequence

    public:
    // SELECTORS & IO FUNCTIONS
    // Writes first n elements of the sequence (all if n == -1) on
    // standard output; format description in definition.
    void write(int n = -1, bool with_n = true);

    // MODIFIERS
    // Inserts new elemnt at the end.
    void push_back(const T&);
};


template <class T>
void Sequence<T>::push_back(const T& v)
{
    seq.push_back(v);
}

// Format description:
// First line: size of sequence (if with_n == true)
// Next line: consecutive elements of the sequence all separated by
// whitespace characters)
template <class T>
void Sequence<T>::write(int n, bool with_n)
{
    if(n == -1)
      n = seq.size();

    int tmp = 0;
    typename std::list<T>::iterator it = seq.begin();

    for(; it != seq.end(); it++)
      {
          tmp++;

          if(n == tmp)
            break;
      }

    it = seq.begin();

    if(it == seq.end())
      {
          if(with_n)
            puts("0");

          puts("");
          return;
      }

    if(with_n)
      printf("%d\n", tmp);

    std::cout<<*it;
    tmp--;

    for(; it != seq.end(); it++)
      {
          if(tmp == 0)
            break;

          std::cout<<" "<<*it;
          tmp--;
      }

    std::cout<<std::endl;
}

#endif /* SEQUENCES_SEQUENCE_H */
